﻿using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Linq;

namespace Vertigo
{
    class Process
    {

        public static string confPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "confV.xml");
        public static XElement conf = XElement.Load(confPath);
        public static string strLastState = conf.Element("app").Value;
        public static string[] Err= {" "};
        public static int anim = System.Convert.ToInt32(conf.Element("anim").Value);

        public static void Init()
        {
  
            string Err1 = "The number of displays does not match the number of rotation settings.";
            string Err2 = "Please ammend values in conf tags <display></display> and <degree></degree>";
            string Err3 = "The number of parameters from conf is greater than the number of available displays";
            
            
            string strDispToRotate = conf.Element("display").Value;
            string strDegToRotate = conf.Element("degree").Value;
            int iDisp = 0;

            // count available display devices on system
            foreach (Screen screen in Screen.AllScreens)
            {
                iDisp++;
            }
            
            //parse data from config XML 
            if (strDispToRotate.Contains(","))
            {
                if (strDegToRotate.Contains(","))
                {
                    string[] spltDisp = strDispToRotate.Split(',');
                    string[] spltDeg = strDegToRotate.Split(',');
                    int[] displays = new int[spltDisp.Length];
                    int[] degrees = new int[spltDeg.Length];

                    // collect error msgs in array
                    if (spltDisp.Length != spltDeg.Length)
                    {   
                        Err[Err.Length-1] = Err1;
                        Array.Resize(ref Err, Err.Length+1); //resize array depending on No. of errors
                        Err[Err.Length-1] = Err2;
                    }
                    else if (spltDisp.Length > iDisp || spltDeg.Length > iDisp)
                    {
                        Err[Err.Length-1] = Err3;
                        Array.Resize(ref Err, Err.Length + 1);
                        Err[Err.Length-1] = Err2;
                    }
                    else
                    {
                        int i = 0;

                        //handle change or go back to default cases
                        if (strLastState == "!") 
                        {
                            foreach (string disp in spltDisp)
                            {
                                displays[i] = System.Convert.ToInt32(disp);
                                degrees[i] = 0;
                                i++;
                                strLastState = "?";
                            }
                        }
                        else
                        {
                            foreach (string disp in spltDisp)
                            {
                                displays[i] = System.Convert.ToInt32(disp);
                                degrees[i] = System.Convert.ToInt32(spltDeg[i]);
                                i++;
                                strLastState = "!";
                            }
                        }
                        //start the process with parsed params
                        Start(displays, degrees, anim);
                    }
                    
                }
                else
                {
                    Err[Err.Length-1] = Err1;
                    Array.Resize(ref Err, Err.Length + 1);
                    Err[Err.Length-1] = Err2;
                }
            }
            else //single display set for rotation
            {
                int[] displays = new int[1];
                int[] degrees = new int[1];
                displays[0] = System.Convert.ToInt32(strDispToRotate);
                if (strDegToRotate.Contains(","))
                {
                    Err[Err.Length] = Err1;
                    Array.Resize(ref Err, Err.Length + 1);
                    Err[Err.Length] = Err2;
                }
                else
                {   //handle change or go back to default cases
                    if (strLastState == "!")
                    {
                        degrees[0] = System.Convert.ToInt32(strDegToRotate);
                        strLastState = "?";
                    }
                    else
                    {
                        degrees[0] = 0;
                        strLastState = "!";
                    }
                    //start the process with parsed params
                    Start(displays, degrees, anim);
                }
                
            }
        }

        public static void Start(int[] displays, int[] degrees, int anim)
        {
            new Thread(() =>
            {

                Thread.CurrentThread.IsBackground = true;
                Display.Orientations thisRotation = new Display.Orientations();
                
                for (int i = 0; i < displays.Length; i++)
                {
                    switch (degrees[i])
                    {
                        case 0:
                            thisRotation = Display.Orientations.DEGREES_CW_0;
                            break;
                        case 90:
                            thisRotation = Display.Orientations.DEGREES_CW_90;
                            break;
                        case 270:
                            thisRotation = Display.Orientations.DEGREES_CW_270;
                            break;
                        case 180:
                            thisRotation = Display.Orientations.DEGREES_CW_180;
                            break;

                    }
                    
                    //call the screen display modifier with parsed paramaters
                    Display.Rotate(Convert.ToUInt32(displays[i]), thisRotation);
                    
                    //save current app state in conf -> ! = default state
                    //                                  ? = modified state 
                    conf.Element("app").Value = strLastState;
                    conf.Save(confPath);
                }

            }).Start();
            
        }
    }
}
